import { Component, OnInit, ViewEncapsulation  } from '@angular/core';
import { Router } from '@angular/router';

import { BookService } from '../services/book.service';
import Book from '../models/book';

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BookCreateComponent implements OnInit {

  book: Book = new Book();

  constructor(
    private router: Router,
    private bookService: BookService
  ) { }

  ngOnInit() {
  }

  saveBook() {
    this.bookService.createBook(this.book)
      .subscribe(res => {
          let id = res['_id'];
          this.router.navigate(['/book-details', id]);
        }, (err) => {
          console.log(err);
        }
      );
  }
}
