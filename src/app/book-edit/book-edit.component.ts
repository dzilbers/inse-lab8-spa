import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BookService } from '../services/book.service';
import Book from '../models/book';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BookEditComponent implements OnInit {

  book: Book = new Book();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private bookService: BookService
  ) { }

  ngOnInit() {
    this.bookService.getBook(this.route.snapshot.params['id'])
      .subscribe( res => {
        this.book = res;
        console.log(res);
      });
  }

  updateBook() {
    this.bookService.updateBook(this.book)
      .subscribe(res => {
          this.book = res;
          console.log(res);
          this.router.navigate(['/book-details', this.book._id]);
        }, (err) => {
          console.log(err);
        }
      );
  }
}
