import { Component, OnInit, ViewEncapsulation  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BookService } from '../services/book.service';
import Book from '../models/book';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BookDetailComponent implements OnInit {

  book: Book = new Book();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private bookService: BookService
  ) { }

  ngOnInit() {
    this.bookService.getBook(this.route.snapshot.params['id'])
      .subscribe(res => {
        this.book = res;
        console.log(res);
      });
  }

  deleteBook(id) {
    this.bookService.deleteBook(id)
      .subscribe(res => {
          this.router.navigate(['/']);
          console.log(res);
        }, (err) => {
          console.log(err);
        }
      );
  }
}
